require 'active_record'

class CreatePlayerAuthenticationMigration < ActiveRecord::Migration[6.1]

  enable_extension 'pgcrypto'

  def change
    create_table :player_authentications, id: :uuid do |t|
      t.belongs_to :user, type: :uuid, foreing_key: true
      t.timestamps null: false
    end
  end
end
