require 'active_record'

module Authentication

  class PlayerAuthentication < ActiveRecord::Base

    belongs_to :user

    def to_hash
      {
        authId: id,
        userId: user_id,
      }
    end
  end
end