module Authentication

  class AppInitializer < RubyPitaya::InitializerBase

    # method:     run
    # parameter:  initializer_content
    # attributes:
    #  - services
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/service_holder.rb
    #
    #  - config
    #    - class: RubyPitaya::Config
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/config.rb
    #    - methods:
    #      - [](key)
    #        - get config file by config path
    #  - setup
    #    - class: RubyPitaya::Setup
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/setup.rb
    #    - methods:
    #      - [](key)
    #        - get config file by config path
    #  - log
    #    - class: Logger
    #    - link: https://ruby-doc.org/stdlib-2.6.4/libdoc/logger/rdoc/Logger.html
    #    - methods:
    #      - info
    #        - log information
    #
    # services:
    #  - redis
    #    - link: https://github.com/redis/redis-rb/
    #
    #  - mongo
    #    - class: Mongo::Client
    #    - link: https://docs.mongodb.com/ruby-driver/current/tutorials/quick-start/

    def run(initializer_content)
      authenticationBLL = AuthenticationBLL.new

      AuthenticationHandler.objects.add(:bll, authenticationBLL)
    end

    def self.path
      __FILE__
    end
  end
end