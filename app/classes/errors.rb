module Authentication

  class PlayerNotFoundError < RubyPitaya::RouteError
    def initialize
      super(StatusCodes::CODE_ERROR_PLAYER_NOT_FOUND, 'Player not found')
    end
  end
end