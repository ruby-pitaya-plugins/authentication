module Authentication

  class AuthenticationHandler < RubyPitaya::HandlerBase

    non_authenticated_actions :createAccount, :authenticate

    def createAccount
      player_auth = PlayerAuthentication.new(user: User.new)
      player_auth.save!

      create_account_data = bll.get_create_account_data(player_auth.user_id)

      @session.uid = player_auth.user_id

      bind_session_response = @postman.bind_session(@session)

      unless bind_session_response.dig(:error, :code).nil?
        return response = {
          code: RubyPitaya::StatusCodes::CODE_AUTHENTICATION_ERROR,
          message: 'Error to create account',
        }
      end

      player_auth_info = player_auth.to_hash
      player_auth_info[:data] = create_account_data unless create_account_data.nil?

      response = {
        code: StatusCodes::CODE_OK,
        data: player_auth_info,
      }
    end

    def authenticate
      auth_id = @params[:authId]
      user_id = @params[:userId]

      player_auth = PlayerAuthentication.where(id: auth_id, user_id: user_id).first
      raise PlayerNotFoundError if player_auth.nil?

      authenticate_data = bll.get_authenticate_data(player_auth.user_id)

      @session.uid = player_auth.user_id

      bind_session_response = @postman.bind_session(@session)

      unless bind_session_response.dig(:error, :code).nil?
        return response = {
          code: RubyPitaya::StatusCodes::CODE_AUTHENTICATION_ERROR,
          msg: 'Error to authenticate',
        }
      end

      player_auth_info = player_auth.to_hash
      player_auth_info[:data] = authenticate_data unless authenticate_data.nil?

      response = {
        code: StatusCodes::CODE_OK,
        data: player_auth_info,
      }
    end

    private

    def bll
      @objects[:bll]
    end

    def response_error(message)
      return response = {
        code: StatusCodes::CODE_ERROR,
        message: message,
      }
    end
  end
end