require 'rspec'
require 'cucumber/rspec/doubles'

require 'rubypitaya/core/helpers/setup_helper'
require 'rubypitaya/core/spec-helpers/app_spec_helper_class'

ENV['RUBYPITAYA_ENV'] = 'test'

RubyPitaya::AppSpecHelper.initialize_before_suite

at_exit do
  RubyPitaya::AppSpecHelper.finalize_after_suite
end