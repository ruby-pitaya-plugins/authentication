Feature: Authenticate

  As a player I want to authenticate with my existing account

  Scenario: Authenticate
    Given the following Authentication:
      | id                                   | user_id                              |
      | 00000000-0000-0000-0000-000000000001 | 00000000-0000-0000-0000-000000000002 |
    When client call route 'rubypitaya.authenticationHandler.authenticate' with params:
      | authId                               | userId                               |
      | 00000000-0000-0000-0000-000000000001 | 00000000-0000-0000-0000-000000000002 |
    Then server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "authId": "00000000-0000-0000-0000-000000000001",
          "userId": "00000000-0000-0000-0000-000000000002",
          "data": {}
        }
      }
      """

  Scenario: Can't authenticate if player not exists
    Given client call route 'rubypitaya.authenticationHandler.authenticate' with params:
      | authId                               | userId                               |
      | 00000000-0000-0000-0000-000000000001 | 00000000-0000-0000-0000-000000000002 |
    Then server should response the following json:
      """
      {
        "code": "RP-AUTH-001",
        "message": "Player not found"
      }
      """
