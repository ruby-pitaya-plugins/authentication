Feature: Create account

  As a player I want to create an account

  Scenario: Create account
    Given client call route 'rubypitaya.authenticationHandler.createAccount'
    Then server should response 'code' as 'RP-200'
