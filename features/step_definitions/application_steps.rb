Given(/^[Tt]he following [Aa]uthentication[s]*[:]*$/) do |table|
  table.hashes.each do |hash|
    hash[:user] = User.new(id: hash[:user_id])
    Authentication::PlayerAuthentication.create(hash)
  end
end